var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 5000));

app.use('/pages', express.static(__dirname + '/pages'));

app.get('/', function(request, response) {
  response.sendFile(__dirname + '/pages/index.html');
});

app.get('/index.html', function(request, response) {
  response.sendFile(__dirname + '/pages/index.html');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
